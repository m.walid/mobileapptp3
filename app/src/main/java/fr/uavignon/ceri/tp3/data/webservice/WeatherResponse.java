package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {
    public final List<Weather> weather=null;
    public final Main main=null;
    public final Wind wind=null;
    public final Clouds clouds=null;
    public final Integer dt=null;
    public static class Weather {
        String description;
        String icon;
    }
    public static class Main {
        int humidity;
        float temp;
    }
    public static class Wind {
        float speed ;
    }
    public static class Clouds {
        int all ;
    }
}
