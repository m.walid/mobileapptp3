package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OWMInterface {
    @GET("/data/2.5/weather")
    Call<WeatherResponse> getForecast(@Query("q")String q, @Query("APIkey") String APIkey);

}
