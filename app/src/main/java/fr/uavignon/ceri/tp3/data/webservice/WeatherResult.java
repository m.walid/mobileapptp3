package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.City;

public class WeatherResult {
    public static void transferInfo(WeatherResponse weatherInfo, City cityInfo){
        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setLastUpdate(weatherInfo.dt);

    }
}
